#!/usr/bin/env -S node --experimental-wasi-unstable-preview1 --no-warnings

const fs = require('fs');
const { WASI } = require('wasi');

if (process.argv.length < 3) {
    console.error('not enough arguments:', process.argv);
    process.exit(2);
}

const wasi = new WASI({
  args: process.argv,
  env: process.env,
  preopens: {
    // '/sandbox': '/some/real/path/that/wasm/can/access'
  }
});
const importObject = { wasi_snapshot_preview1: wasi.wasiImport };

const f = process.argv[2];

const mod = new WebAssembly.Module(fs.readFileSync(f));
const instance = new WebAssembly.Instance(mod, importObject);

wasi.start(instance);
